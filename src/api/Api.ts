import { Configuration } from '../features/configurationSlice';
import axios, { AxiosResponse } from 'axios';
import { BacktestResult } from '../features/resultSlice';

export const backtestRequest = (configuration: Configuration): Promise<AxiosResponse<BacktestResult>> => {

  const finalConfiguration: Configuration = {
    ...configuration, configuration: {
      ...configuration.configuration,
      feesSchedule: {
        maker: configuration.configuration.feesSchedule.maker / 100,
        taker: configuration.configuration.feesSchedule.taker / 100,
      },
    },
  };

  const url = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? 'http://localhost:8081/api/backtest/simulation'
    : 'https://backtest-api.deepn.io/api/backtest/simulation';

  return axios.post<BacktestResult>(url, finalConfiguration);
};
