import {
  makeStyles,
  Theme,
} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Chart from './chart';
import {
  ReflexContainer,
  ReflexElement,
  ReflexSplitter,
} from 'react-reflex';

import 'react-reflex/styles.css';
import ResultTabs from './result';
import ConfigurationTabs from './strategy';
import { IconButton } from '@material-ui/core';
import { backtestRequest } from './api/Api';
import {
  useAppDispatch,
  useAppSelector,
} from './hooks';
import { useState } from 'react';
import clsx from 'clsx';
import {
  setBacktestResult,
  setBacktestResultError,
} from './features/resultSlice';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    height: 'calc(100vh - 5vh)',
  },
  page: {
    height: '100%',
    backgroundColor: 'red',
  },
  header: {
    backgroundColor: '#8C40FF',
    height: '5vh',
  },
  toolbar: {},
  logo: {
    marginLeft: '1%',
    width: '120px',
    height: '100%',
  },
  executionButton: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    zIndex: 999,
    margin: theme.spacing(1),
    borderRadius: '100%',
    backgroundColor: '#8C40FF',
    '&:hover': {
      backgroundColor: '#8C40FF',

    },
  },
  animatedButton: {
    transition: 'transform 2s ease-in-out',
    transform: 'rotate(720deg)',
  },
  executionImage: {
    height: 35,
    width: 35,
    padding: 5,
    color: 'white',
  },
  noScroll: {
    overflow: 'hidden',
  },
}));


export default function App() {
  const classes = useStyles();

  // const [loaded, setLoaded] = useState(true)
  const [pendingRequest, setPendingRequest] = useState(false);
  const configuration = useAppSelector((state) => state.configuration);
  const dispatch = useAppDispatch();

  // window.onload = (e) => {
  //     setTimeout(() => setLoaded(true), 200)
  // }

  const backtest = () => {
    setPendingRequest(true);
    backtestRequest(configuration.value)
      .then(value => {
        dispatch(setBacktestResult(value.data));
        dispatch(setBacktestResultError(null));
      }).catch(error => dispatch(setBacktestResultError(error.response.data)));
  };

  return (
    <div className={classes.root}>
      {/*<Loader display={(!loaded)}/>*/}
      <IconButton aria-label="execute"
                  className={classes.executionButton}
                  size={'medium'}
                  onClick={() => backtest()}>
        <img alt={'execute'}
             src={'icons/launch.svg'}
             className={clsx(classes.executionImage, { [classes.animatedButton]: pendingRequest })} />
      </IconButton>
      <AppBar position="static"
              className={classes.header}>
        <img src={'logo.svg'}
             alt={'logo'}
             className={classes.logo} />
      </AppBar>
      <ReflexContainer orientation="horizontal">
        <ReflexElement>
          <Chart />
        </ReflexElement>
        <ReflexSplitter />
        <ReflexElement className={classes.noScroll}>
          <ReflexContainer orientation="vertical"
                           className={classes.noScroll}>
            <ReflexElement className={classes.noScroll}>
              <ConfigurationTabs />
            </ReflexElement>
            <ReflexSplitter />
            <ReflexElement>
              <ResultTabs />
            </ReflexElement>
          </ReflexContainer>
        </ReflexElement>
      </ReflexContainer>
    </div>
  );
}
