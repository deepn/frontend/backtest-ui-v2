import React, {useEffect, useState} from 'react'
import {makeStyles} from "@material-ui/core/styles";
import {Typography} from "@material-ui/core";
import clsx from "clsx";


const useStyles = makeStyles(_ => ({
    root: {
        zIndex: 9999,
        position: 'absolute',
        height: '100vh',
        width: '100%',
        backgroundColor: 'rgba(255,255,255)',
        display: 'flex',
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    image: {
        height: '512px'
    },
}));

interface LoaderProps {
    display: boolean
}

export default function Loader(props: LoaderProps) {
    const classes = useStyles()

    const [show, setShow] = useState(true)
    const [loadingText, setLoadingText] = useState("Loading engine")

    useEffect(() => {
        if (!props.display)
            setTimeout(() => setShow(false), 3000)
    }, [props.display])

    const countPoint = (text: string) => {
        const count = (text.match(/\./g) || []).length + 1
        if (count > 3) return 0
        return count
    }
    useEffect(() => {
        if (props.display)
            setTimeout(() =>
                setLoadingText("Loading engine" + Array(countPoint(loadingText)).fill(".").join("")), 500)
    }, [loadingText, props.display])

    if (show) {
        return (
            <div className={clsx(classes.root, {"fade-out": !props.display})}>
                <img src={"/loader.gif"} alt={'loader'} className={classes.image}/>
                <Typography className='loading' variant={'h4'}>{loadingText}</Typography>
            </div>
        )
    } else return (<></>)
}