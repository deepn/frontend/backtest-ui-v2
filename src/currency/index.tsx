import React from "react";
import {createStyles, withStyles} from "@material-ui/core/styles";
import {fade, InputBase, Paper, Typography} from "@material-ui/core";
import {numberOrZero} from "../utils";
import {Line, LineChart, ResponsiveContainer} from "recharts";
import {TrendData} from "../data/trends";
import classNames from "classnames";
import {WalletCurrency} from "../features/configurationSlice";

export const getImageSource = (currency: String) =>
    `https://raw.githubusercontent.com/alexandrebouttier/coinmarketcap-icons-cryptos/main/icons/${currency.toLowerCase()}.png`

export const TRENDS_COLOR = ["#357a38", "#ff1744", "#6B7789"]

interface IconImageProps {
    id?: string | null
    currency: string | null | String;
    classes: any;
    height?: number | string | null,
    width?: number | string | null,
    style?: object,
    className?: string
}

export const CurrencyIcon = withStyles(() =>
    createStyles({
        currencyIcon: {
            marginRight: 5
        }
    })
)((props: IconImageProps) => {
    const {id, currency, height, width, style, className, classes} = props
    const notFoundSource = "icons/icon_not_found.svg"
    if (!currency) return <img className={classes.currencyIcon} src={notFoundSource} alt={"not found"}/>

    return <img
        id={id || currency.toLowerCase() + new Date().getTime()}
        className={classNames(classes.currencyIcon, className || [])}
        src={getImageSource(currency)}
        alt={currency.toUpperCase()}
        crossOrigin={"anonymous"}
        style={{height: height || 18, width: width || 18, ...style}}
        onError={(image) => image.currentTarget.src = notFoundSource}
    />
})

interface CryptoWalletItemProps {
    currency: string | String;
    classes: any;
    onChange: (_: WalletCurrency) => void,
    data?: WalletCurrency
}

export const CryptoWalletItem = withStyles(() =>
    createStyles({
        currencyPaper: {
            minWidth: 120,
            width: 180,
            height: 40,
            padding: 10,
            display: 'flex',
            justifyItems: 'center',
            alignItems: 'center',
            flexDirection: 'row',
            margin: 5
        },
        quantityEditor: {
            minWidth: 0,
            marginLeft: 10,
            marginRight: 10,
        },
        text: {
            minWidth: 40,
            textAlign: 'left',
        }

    })
)((props: CryptoWalletItemProps) => {
    const {currency, data, onChange, classes} = props

    return <Paper className={classes.currencyPaper} elevation={3}>
        <CurrencyIcon id={`wallet_${currency.toUpperCase()}`} currency={currency} height={30} width={30}/>
        <InputBase value={(data ? data.quantity : 0)} type={"number"}
                   className={classes.quantityEditor}
                   inputProps={{style: {textAlign: 'center'}}}
                   onChange={(e) => onChange({currency, quantity: numberOrZero(e.target.value)})}
        />
        <Typography variant="subtitle1" className={classes.text}>{currency.toUpperCase()}</Typography>
    </Paper>
})

interface TrendClickCallback {
    onClick: (_: TrendData) => void
}

export const CryptoPeriodItem = withStyles(() =>
    createStyles({
        currencyPaper: {
            width: "80%",
            minHeight: 70,
            display: 'flex',
            justifyItems: 'center',
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
            '&:hover': {
                backgroundColor: 'rgba(140, 64, 255, 0.1)',
                cursor: 'pointer'
            },
            marginBottom: 10
        },
        dateContent: {
            display: 'flex',
            flexDirection: 'row',
            justifyItems: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
            marginLeft: 20,
        },
        leftCurrencyIcon: {
            transform: 'rotate(12deg)',
            marginRight: 0,
            zIndex: 1
        },
        rightCurrencyIcon: {
            transform: 'rotate(-12deg)',
            marginLeft: -8,
        },
        tag: {
            position: 'absolute',
            top: 5,
            right: 5,
            padding: '3px 10px 3px 10px',
            fontSize: 'x-small',
            textAlign: 'center',
            color: 'white',
            backgroundColor: fade('#979797', 0.8),
            borderRadius: 4,
            zIndex: 999
        },
        content: {
            display: 'flex',
            flexDirection: 'row',
            width: '100%',
            height: '100%',
            position: 'relative'
        },
        currencyText: {
            color: '#9FA6BB'
        }
    })
)((props: TrendData & TrendClickCallback & {key: string}) => {
    const {base, quote, data, trend, timeFrame, onClick, classes} = props

    return <Paper key={base+quote} className={classes.currencyPaper} elevation={2} onClick={() => onClick(props)}>
        <div className={classes.content}>
            <div className={classes.tag}>
                {timeFrame.toUpperCase()}
            </div>
            <div style={{
                height: '100%',
                width: '30%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column'
            }}>
                <div style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: 5
                }}>
                <CurrencyIcon className={classes.leftCurrencyIcon} currency={base} height={25} width={25}/>
                <CurrencyIcon className={classes.rightCurrencyIcon} currency={quote} height={25} width={25}/>
                </div>

                <Typography variant={'caption'} className={classes.currencyText}>{base}/{quote}</Typography>
            </div>

            <ResponsiveContainer width={'70%'}>
                <LineChart data={data}>
                    <Line type="linear" dataKey="open" stroke={TRENDS_COLOR[trend]} dot={false}/>
                </LineChart>
            </ResponsiveContainer>
        </div>
    </Paper>
})