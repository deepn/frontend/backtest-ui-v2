import { TextField } from '@material-ui/core';
import { CurrencyIcon } from '../currency';
import { numberOrZero } from '../utils';
import React from 'react';
import {
  createStyles,
  makeStyles,
} from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    textField: {
      margin: 10,
      minWidth: 250,
    },
  }),
);

export interface NumberInputProps {
  value?: number,
  label: string,
  showQuote?: boolean
  isPercentage?: boolean,
  onChange: (_: any) => void,
  selectedQuote?: string,
  disabled?: boolean
}

export const NumberInput = ({
                              value,
                              label,
                              showQuote,
                              isPercentage,
                              selectedQuote,
                              onChange,
                              disabled = false,
                            }: NumberInputProps) => {
  const classes = useStyles();
  return (<TextField className={classes.textField}
                     disabled={disabled}
                     InputProps={{
                       endAdornment: (isPercentage ? <>%</> : (showQuote ?
                         <CurrencyIcon currency={selectedQuote!} /> : null)),
                     }}
                     value={value}
                     type={'number'}
                     label={label}
                     variant={'outlined'}
                     onChange={(event) => onChange(numberOrZero(event.target.value))}
  />);
};
