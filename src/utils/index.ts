import Vibrant from 'node-vibrant';

export const numberOrZero = (value: string) => {
  let number = 0;
  try {
    number = parseFloat(value);
    if (isNaN(number))
      number = 0;
  } catch (e) {
  }
  return number;
};

export interface ImageColorResult {
  currency: string,
  color: string
}

export const getImageColor = async (image: HTMLImageElement): Promise<ImageColorResult> => {
  if (!image) return Promise.resolve({ currency: 'null', color: '#000000' });
  return new Promise((resolve, reject): void => {
    image.addEventListener(
      'load',
      async (): Promise<void> => {
        const vibrant = new Vibrant(image);
        const palette = await vibrant.getPalette();
        resolve({ currency: image.alt, color: palette.Vibrant?.hex || '#000000' });
      },
    );
  });
};

export const getImageColorFromURL = async (currency: string | String, image: string): Promise<ImageColorResult> => {
  if (!image) return Promise.resolve({ currency: 'null', color: '#000000' });
  return new Promise(async (resolve, reject) => {
    const vibrant = new Vibrant(image);
    const palette = await vibrant.getPalette();
    resolve({ currency: currency as string, color: palette.Vibrant?.hex || '#000000' });
  });
};

export const validateNumbers = (values: Array<number>): boolean => {
  return values.every(v => v != null && isFinite(v));
};

export function formatDuration(seconds: number) {
  let numYears = Math.floor(seconds / (86400 * 365));
  let numDays = Math.floor(seconds % (86400 * 365) / 86400);
  let numHours = Math.floor(((seconds % (86400 * 365)) % 86400) / 3600);
  let numMinutes = Math.floor((((seconds % (86400 * 365)) % 86400) % 3600) / 60);
  let numSeconds = (((seconds % (86400 * 365)) % 86400) % 3600) % 60;

  let secondsFun = function () {
    return (numSeconds ? (numSeconds + 's') : '');
  };

  let minutesFun = function () {
    return (numMinutes ? numMinutes + 'm ' : '');
  };

  let hoursFun = function () {
    if (minutesFun().charAt(0) === 'a') {
      return (numHours ? numHours + 'h ' : '');
    }
    if (numSeconds === 0 && numMinutes === 0) {
      return (numHours ? numHours + 'h ' : '');
    }

    return (numHours ? numHours + 'h ' : '');
  };

  let daysFun = function () {
    return (numDays ? numDays + 'd ' : '');
  };

  let yearsFun = function () {
    return (numYears ? numYears + 'y ' : '');
  };

  return yearsFun() + daysFun() + hoursFun() + minutesFun() + secondsFun();
}

