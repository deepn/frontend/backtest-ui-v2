import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core";
import {Provider} from "react-redux";
import {store} from "./store";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#8C40FF",
            light: "#A366FF",
            dark: "#8C40FF",
        },
        secondary: {
            main: "#89D1F6",
            light: "#5ABFF2",
            dark: "#2BADEE",
        }
    }
});

ReactDOM.render(
    <React.StrictMode>
        <MuiThemeProvider theme={theme}>
            <Provider store={store}>
                <App/>
            </Provider>
        </MuiThemeProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

