import React, {
  useEffect,
  useRef,
  useState,
} from 'react';
import CodeMirrorEditor from 'react-codemirror';

import './style/codemirror.css';
import './language/deepscript';
import './style/show-hint.css';
import {
  useAppDispatch,
  useAppSelector,
} from '../../hooks';
import {
  getStrategy,
  setStrategy,
} from '../../features/configurationSlice';
import { getErrors } from '../../features/resultSlice';
import { TextMarker } from 'codemirror';


export default function DeepScriptEditor() {

  const strategy = useAppSelector(getStrategy);
  const errors = useAppSelector(getErrors);

  const [code, setCode] = useState<string>(strategy);

  const dispatch = useAppDispatch();

  const codeMirrorReference = useRef<any>();
  const lastMarker = useRef<Array<TextMarker>>([]);

  useEffect(() => {
    if (lastMarker.current) {
      lastMarker.current.forEach(m => m.clear());
      lastMarker.current = [];
    }

    if (errors && errors.error && codeMirrorReference.current) {
      const error = errors.error.value.error;

      lastMarker.current.push(codeMirrorReference.current.getCodeMirror().getDoc().markText({
        line: error.start ? error.start.line - 1 : error.line - 1,
        ch: error.start ? error.start.charPositionInLine : 0,
      }, {
        line: error.stop ? error.stop.line - 1 : error.line - 1,
        ch: error.stop ? error.stop.charPositionInLine : 10000,
      }, { className: 'err', title: error.exception ? error.exception : 'Syntax error' }));
      
    }
  }, [errors, codeMirrorReference]);

  const saveCode = () => {
    dispatch(setStrategy(code));
  };

  const setReference = (reference: any) => {
    codeMirrorReference.current = reference;
    let instance = reference.getCodeMirrorInstance();
    require('./addons/show-hint.js')(instance);
    require('./language/ds-hint')(instance);
    require('./language/ds-hint')(instance);

  };


  const autoComplete = (_: any) => {
    let instance = codeMirrorReference.current?.getCodeMirrorInstance();
    instance?.showHint(_, instance.hint.deepscript);
  };

  return (
    <CodeMirrorEditor
      ref={(e: any) => e && setReference(e)}
      value={code}
      onChange={code => setCode(code)}
      onFocusChange={() => saveCode()}
      options={{
        mode: 'deepscript',
        lineNumbers: true,
        smartIndent: true,
        scrollbarStyle: 'native',
        extraKeys: {
          'Ctrl-Space': autoComplete,
        },
      }} />
  );
}
