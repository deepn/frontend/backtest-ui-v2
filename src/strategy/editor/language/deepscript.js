(function (mod) {
    if (typeof exports == "object" && typeof module == "object") // CommonJS
        mod(require("codemirror"));
    else if (typeof define == "function" && define.amd) // AMD
        define(["codemirror"], mod);
    else // Plain browser env
        mod(CodeMirror);
})(function (CodeMirror) {
    "use strict";

    CodeMirror.defineMode("deepscript", function (config, parserConfig) {
        var indentUnit = config.indentUnit;

        function prefixRE(words) {
            return new RegExp("^(?:" + words.join("|") + ")", "i");
        }

        function wordRE(words) {
            return new RegExp("^(?:" + words.join("|") + ")$", "i");
        }

        const specials = wordRE(["close", "high", "open", "low", "volume", "bar_index", "series",
            "position_size", "OrderStatus", "OrderSide", "OrderType"]);

        const builtins = wordRE(['print', 'abs', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil', 'cos', 'cosh', 'exp', 'expm1', 'float', 'floor', 'hypot', 'int', 'ln', 'ln1p', 'log', 'log10', 'log2', 'max', 'min', 'round', 'sign', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'truncate', 'aroonDown', 'aroonOscillator', 'aroonUp', 'atr', 'cci', 'chandelierExitLong', 'chandelierExitShort', 'chop', 'cmo', 'coppockCurve', 'crossOver', 'crossUp', 'crossOver', 'crossUp', 'crossUnder', 'crossDown', 'crossUnder', 'crossDown', 'dema', 'dpo', 'dpo', 'ema', 'ha', 'heikinAshi', 'hma', 'kama', 'lwma', 'macd', 'massIndex', 'mma', 'parabolicSar', 'ppo', 'ravi', 'rma', 'roc', 'rsi', 'sma', 'stochD', 'stochK', 'stochK', 'stochRSI', 'tema', 'ulcerIndex', 'williamR', 'wma', 'zlema', 'isNaN', 'na', 'len', 'length', 'size', 'type', 'append', 'append', 'push', 'pushBack', 'array', 'clear', 'contains', 'get', 'prepend', 'prepend', 'pushFront', 'appendLeft', 'pushAt', 'remove', 'removeAll', 'removeAt', 'nz']
        );
        const keywords = wordRE(["else", "elseif", "in", "until", "for", "while", "if", "@", "static", "function", "end", "return", "and", "or", "break", "continue"]);

        const operators = wordRE(["\\=", "\\=<", "\\>", "\\<", "\\=>", "\\+", "\\-", "\\/", "\\*"]);

        const indentTokens = wordRE(["function", "if", "else", "elseif"]);
        const dedentTokens = wordRE(["end", "until"]);
        const dedentPartial = prefixRE(["until", "else", "elseif"]);

        const functionContext = "function"
        const exitFunctionContext = "end"

        function readBracket(stream) {
            let level = 0;
            while (stream.eat("=")) ++level;
            stream.eat("[");
            return level;
        }

        function normal(stream, state) {
            const ch = stream.next();
            if (ch === "/" && stream.eat("/")) {
                if (stream.eat("[") && stream.eat("["))
                    return (state.cur = bracketed(readBracket(stream), "comment"))(stream, state);
                stream.skipToEnd();
                return "comment";
            }

            if (ch === "@") {
                stream.skipTo("(");
                return "variable-3";
            }

            if (ch === "\"" || ch === "'")
                return (state.cur = string(ch))(stream, state);
            if (ch === "[" && /[\[=]/.test(stream.peek()))
                return (state.cur = bracketed(readBracket(stream), "string"))(stream, state);
            if (/\d/.test(ch)) {
                stream.eatWhile(/[\w.%]/);
                return "number";
            }
            if (/[\w_]/.test(ch)) {
                if (stream.pos > 2 && stream.string[stream.pos - 2] === '.') {
                    stream.eatWhile(/[\w\\\-_]/);
                    return "object-variable";
                }

                if (/[a-zA-Z]+\([^\)]*\)(\.[^\)]*\))?/y.test(stream.string.substring(stream.pos - 1))) {
                    stream.eatWhile(/[\w\\\-_]/);
                    return "function-call"
                }

                stream.eatWhile(/[\w\\\-_]/);
                return "variable";
            }
            return null;
        }

        function bracketed(level, style) {
            return function (stream, state) {
                var curlev = null, ch;
                while ((ch = stream.next()) != null) {
                    if (curlev == null) {
                        if (ch === "]") curlev = 0;
                    } else if (ch === "=") ++curlev;
                    else if (ch === "]" && curlev === level) {
                        state.cur = normal;
                        break;
                    } else curlev = null;
                }
                return style;
            };
        }

        function string(quote) {
            return function (stream, state) {
                var escaped = false, ch;
                while ((ch = stream.next()) != null) {
                    if (ch === quote && !escaped) break;
                    escaped = !escaped && ch === "\\";
                }
                if (!escaped) state.cur = normal;
                return "string";
            };
        }

        return {
            startState: function (basecol) {
                return {basecol: basecol || 0, indentDepth: 0, cur: normal};
            },

            token: function (stream, state) {
                if (stream.eatSpace()) return null;
                var style = state.cur(stream, state);
                var word = stream.current();

                if (word === functionContext || word === exitFunctionContext)
                    state.inFunction = (word === functionContext)


                if (!style && operators.test(word)) {
                    style = "operator"
                }

                if (keywords.test(word)) style = "keyword";

                if (style === "variable" ) {
                    if (!state.inFunction && specials.test(word)) style = "variable-2";
                }

                if (style === "function-call") {
                    if (builtins.test(word)) style = "builtin";
                }

                if ((style !== "comment") && (style !== "string")) {
                    if (indentTokens.test(word)) {
                        ++state.indentDepth;
                    }
                    else if (dedentTokens.test(word)) {
                        --state.indentDepth;
                    }
                }
                return style;
            },

            indent: function (state, textAfter) {
                var closing = dedentPartial.test(textAfter);
                return state.basecol + indentUnit * (state.indentDepth - (closing ? 1 : 0));
            },

            electricInput: /^\s*(?:until|else|\)|\})$/,
            lineComment: "//",
            blockCommentStart: "/**",
            blockCommentEnd: "**\\"
        };
    });

    CodeMirror.defineMIME("text/x-deepscript", "deepscript");

});
