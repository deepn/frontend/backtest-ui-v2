// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: https://codemirror.net/LICENSE

module.exports = (function(CodeMirror) {
    var Pos = CodeMirror.Pos;

    function forEach(arr, f) {
        for (var i = 0, e = arr.length; i < e; ++i) f(arr[i]);
    }

    function arrayContains(arr, item) {
        if (!Array.prototype.indexOf) {
            var i = arr.length;
            while (i--) {
                if (arr[i] === item) {
                    return true;
                }
            }
            return false;
        }
        return arr.indexOf(item) != -1;
    }

    function scriptHint(editor, keywords, getToken, options) {
        // Find the token at the cursor
        var cur = editor.getCursor(), token = getToken(editor, cur);
        if (/\b(?:string|comment)\b/.test(token.type)) return;
        var innerMode = CodeMirror.innerMode(editor.getMode(), token.state);
        if (innerMode.mode.helperType === "json") return;
        token.state = innerMode.state;

        // If it's not a 'word-style' token, ignore the token.
        if (!/^[\w$_]*$/.test(token.string)) {
            token = {start: cur.ch, end: cur.ch, string: "", state: token.state,
                type: token.string == "." ? "property" : null};
        } else if (token.end > cur.ch) {
            token.end = cur.ch;
            token.string = token.string.slice(0, cur.ch - token.start);
        }

        var tprop = token;
        // If it is a property, find out what it is a property of.
        while (tprop.type == "property") {
            tprop = getToken(editor, Pos(cur.line, tprop.start));
            if (tprop.string != ".") return;
            tprop = getToken(editor, Pos(cur.line, tprop.start));
            if (!context) var context = [];
            context.push(tprop);
        }
        return {list: getCompletions(token, context, keywords, options),
            from: Pos(cur.line, token.start),
            to: Pos(cur.line, token.end)};
    }

    function javascriptHint(editor, options) {
        return scriptHint(editor, keywords,
            function (e, cur) {return e.getTokenAt(cur);},
            options);
    };

    CodeMirror.registerHelper("hint", "deepscript", javascriptHint);

    var stringProps = ("").split(" ");
    var arrayProps = ("bb").split(" ");
    var funcProps = "".split(" ");
    var keywords = ("for function @buy @sell abs acos acosh asin asinh atan atan2 atanh ceil cos cosh exp expm1 float floor hypot int ln ln1p log log10 log2 max min round sign sin sinh sqrt tan tanh truncate aroonDown aroonOscillator aroonUp atr cci chandelierExitLong chandelierExitShort chop cmo coppockCurve crossOver crossUp crossOver crossUp crossUnder crossDown crossUnder crossDown dema dpo dpo ema ha heikinAshi hma kama lwma macd massIndex mma parabolicSar ppo ravi rma roc rsi sma stochD stochK stochK stochRSI tema ulcerIndex williamR wma zlema isNaN na len length size type append append push pushBack array clear contains get prepend prepend pushFront appendLeft pushAt remove removeAll removeAt").split(" ");


    function getCompletions(token, context, keywords, options) {

        var found = [], start = token.string, global = options && options.globalScope || window;
        function maybeAdd(str) {
            if (str.lastIndexOf(start, 0) == 0 && !arrayContains(found, str)) found.push(str);
        }
        function gatherCompletions(obj) {
            if (typeof obj == "string") forEach(stringProps, maybeAdd);
            else if (obj instanceof Array) forEach(arrayProps, maybeAdd);
            else if (obj instanceof Function) forEach(funcProps, maybeAdd);
           // forAllProps(obj, maybeAdd)
        }

        if (context && context.length) {
            // If this is a property, see if it belongs to some object we can
            // find in the current environment.
            var obj = context.pop(), base;
            if (obj.type && obj.type.indexOf("variable") === 0) {
                if (options && options.additionalContext)
                    base = options.additionalContext[obj.string];
                if (!options || options.useGlobalScope !== false)
                    base = base || global[obj.string];
            } else if (obj.type == "string") {
                base = "";
            } else if (obj.type == "atom") {
                base = 1;
            } else if (obj.type == "function") {
                if (global.jQuery != null && (obj.string == '$' || obj.string == 'jQuery') &&
                    (typeof global.jQuery == 'function'))
                    base = global.jQuery();
                else if (global._ != null && (obj.string == '_') && (typeof global._ == 'function'))
                    base = global._();
            }
            while (base != null && context.length)
                base = base[context.pop().string];
            if (base != null) gatherCompletions(base);
        } else {
            // If not, just look in the global object, any local scope, and optional additional-context
            // (reading into JS mode internals to get at the local and global variables)
            for (var v = token.state.localVars; v; v = v.next) maybeAdd(v.name);
            for (var c = token.state.context; c; c = c.prev)
                for (var v = c.vars; v; v = v.next) maybeAdd(v.name)
            for (var v = token.state.globalVars; v; v = v.next) maybeAdd(v.name);
            if (options && options.additionalContext != null)
                for (var key in options.additionalContext)
                    maybeAdd(key);
            if (!options || options.useGlobalScope !== false)
                gatherCompletions(global);
            forEach(keywords, maybeAdd);
        }
        return found;
    }
});