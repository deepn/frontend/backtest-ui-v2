import React from 'react';
import {
  createStyles,
  makeStyles,
} from '@material-ui/core/styles';
import {
  useAppDispatch,
  useAppSelector,
} from '../../../../hooks';
import {
  getFeesSchedule,
  getQuote,
  getTradeConfiguration,
  setMakerFees,
  setMaximumOpenOrder,
  setMaximumOpenOrderPerCoin,
  setMaximumQuantity,
  setMinimumQuantity,
  setTakerFees,
} from '../../../../features/configurationSlice';
import { NumberInput } from '../../../../inputs/NumberInput';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      justifyItems: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      height: '100%',
    },
    group: {
      borderRadius: 5,
      display: 'flex',
      justifyContent: 'center',
      justifyItems: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      padding: 15,
      position: 'relative',
      margin: 10,
      width: '60%',
    },
    groupLine: {
      display: 'flex',
      justifyContent: 'center',
      justifyItems: 'center',
      alignItems: 'center',
      flexDirection: 'row',
    },
    groupLabel: {
      position: 'absolute',
      left: 20,
      top: 0,
      transform: 'translateY(-50%)',
      backgroundColor: 'white',
      paddingLeft: 5,
      paddingRight: 5,
    },
  }),
);

export default function TradingConfiguration() {
  const classes = useStyles();
  const selectedQuote = useAppSelector(getQuote);

  const feesSchedule = useAppSelector(getFeesSchedule);
  const tradingConfiguration = useAppSelector(getTradeConfiguration);

  const dispatch = useAppDispatch();

  return (
    <div className={classes.root}>

      <div className={classes.group}>
        <div className={classes.groupLine}>
          <NumberInput value={feesSchedule.maker}
                       label="Maker fees"
                       isPercentage
                       onChange={v => dispatch(setMakerFees(v))} />
          <NumberInput value={feesSchedule.taker}
                       label="Taker fees"
                       isPercentage
                       onChange={v => dispatch(setTakerFees(v))} />
        </div>
        <div className={classes.groupLine}>
          <NumberInput selectedQuote={selectedQuote}
                       value={tradingConfiguration.minimumQuantity}
                       label="Min. quantity"
                       showQuote
                       onChange={v => dispatch(setMinimumQuantity(v))} />
          <NumberInput selectedQuote={selectedQuote}
                       value={tradingConfiguration.maximumQuantity}
                       label="Max. quantity"
                       showQuote
                       onChange={v => dispatch(setMaximumQuantity(v))} />
        </div>
        <div className={classes.groupLine}>
          <NumberInput value={tradingConfiguration.maximumOpenOrders}
                       label="Max. open orders"
                       onChange={v => dispatch(setMaximumOpenOrder(v))} />
          <NumberInput value={tradingConfiguration.maximumOrderPerCoin}
                       label="Max. open orders / Currency"
                       onChange={v => dispatch(setMaximumOpenOrderPerCoin(v))} />
        </div>
      </div>
    </div>
  );

}
