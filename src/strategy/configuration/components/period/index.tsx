import React, {useState} from 'react'
import 'react-date-range/dist/styles.css';
import './theme/light.css';
import {DateRange} from 'react-date-range';
import {createStyles, makeStyles} from "@material-ui/core/styles";
import {CryptoPeriodItem} from "../../../../currency";
import {TrendData, TRENDS} from "../../../../data/trends";
import Carousel, {arrowsPlugin, slidesToScrollPlugin, slidesToShowPlugin} from "@brainhubeu/react-carousel";
import '@brainhubeu/react-carousel/lib/style.css';
import {ChevronLeft, ChevronRight} from "@material-ui/icons";
import clsx from "clsx";
import {Typography} from "@material-ui/core";
import {useAppDispatch, useAppSelector} from "../../../../hooks";
import {getPeriod, getTimeFrame, setPeriod, setTimeFrame} from "../../../../features/configurationSlice";

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            display: 'flex',
            justifyItems: 'center',
            flexDirection: 'row',
            height: '100%',
        },
        scenario: {
            height: '100%',
            flex: '1 0 40%',
            display: 'flex',
            flexDirection: 'column',
            justifyItems: 'center',
            alignItems: 'center',
            width: '100%',
            paddingTop: 10,
            color: '#6B7789',
        },
        builtInPeriod: {
            overflow: 'auto',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            justifyItems: 'center',
            alignItems: 'center',
            width: '100%'
        },
        timeFrame: {
            color: '#A366FF',
            cursor: 'pointer',
            border: '1px solid #A366FF',
            borderRadius: 5,
            padding: 5,
            minWidth: 40,
            marginRight: 5,
            textAlign: 'center',
            '&:hover': {
                border: '1px solid #8C40FF',
                backgroundColor: '#A366FF',
                color: 'white'
            }
        },
        timeFrameSelected: {
            border: '1px solid #8C40FF',
            backgroundColor: '#A366FF',
            color: 'white'
        },
        carouselIcon: {
            color: '#A366FF',
            '&:hover': {
                borderRadius: 5,
                backgroundColor: '#A366FF',
                color: 'white'
            }
        },
        content: {
            display: 'flex',
            flexDirection: 'column',
            flex: '2 1 60%',
            width: '60%'
        },
        carousel : {
            paddingTop: 10,
        }
    })
);

export default function PeriodSelector() {
    const classes = useStyles()

    const [scenarioOffset, setScenarioOffset] = useState(10)

    const period = useAppSelector(getPeriod)
    const timeFrame = useAppSelector(getTimeFrame)

    const dispatch = useAppDispatch()

    const handleScroll = (event: any) => {
        const target = event.target
        if (target.scrollHeight - target.scrollTop === target.clientHeight) {
            setScenarioOffset(Math.min(scenarioOffset + 10, TRENDS.length - 1))
        }
    }

    const selectScenario = (trend: TrendData) => {
        dispatch(setTimeFrame(trend.timeFrame))
        dispatch(setPeriod({startTime: trend.startTime, endTime: trend.endTime}))
    }

    return (
        <div className={classes.root}>
            <div className={classes.scenario}>
                <Typography variant={'h6'} align={'center'}>Scenario</Typography>
                <div className={classes.builtInPeriod} onScroll={handleScroll}>
                    {TRENDS.slice(0, scenarioOffset).map(trend =>
                        <CryptoPeriodItem key={"trend_" + trend.quote + trend.base + trend.startTime + trend.endTime} {...trend} onClick={selectScenario}/>
                    )}
                </div>
            </div>
            <div className={classes.content}>
                <Carousel
                    className={classes.carousel}
                    plugins={[
                        {
                            resolve: slidesToShowPlugin,
                            options: {numberOfSlides: 6}
                        },
                        {
                            resolve: slidesToScrollPlugin,
                            options: {numberOfSlides: 6}
                        },
                        {
                            resolve: arrowsPlugin,
                            options: {
                                arrowLeft: <ChevronLeft className={classes.carouselIcon}/>,
                                arrowLeftDisabled: <ChevronLeft className={classes.carouselIcon}/>,
                                arrowRight: <ChevronRight className={classes.carouselIcon}/>,
                                arrowRightDisabled: <ChevronRight className={classes.carouselIcon}/>,
                                addArrowClickHandler: true,
                            }
                        }
                    ]}
                >
                    {["1m", "3m", "5m", "15m", "30m", "1h", "2h", "4h", "6h", "8h", "12h", "1d", "3d", "1w"].map(tf => (
                        <div
                            key={tf}
                            className={clsx(classes.timeFrame, {[classes.timeFrameSelected]: timeFrame === tf})}
                            onClick={_ => dispatch(setTimeFrame(tf))}>
                            {tf}
                        </div>
                    ))}
                </Carousel>
                <DateRange
                    showDateDisplay={false}
                    onChange={({selection}: any) => dispatch(setPeriod({startTime: selection.startDate.getTime(), endTime: selection.endDate.getTime()}))}
                    showSelectionPreview={true}
                    maxDate={new Date()}
                    moveRangeOnFirstSelection={false}
                    months={1}
                    ranges={[{ startDate: new Date(period.startTime), endDate: new Date(period.endTime), key: 'selection'}]}
                    direction="horizontal"
                />
            </div>
        </div>
    )

}
