import React, {useCallback, useEffect, useState} from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete';
import {Avatar, Chip, CircularProgress, TextField} from "@material-ui/core";
import {createStyles, makeStyles} from "@material-ui/core/styles";
import {Alert} from "@material-ui/lab";
import classNames from 'classnames';
import {LimitedListComponent, LimitedListRenderGroup} from "../../../../list/LimitedSizeList";
import {useAppDispatch, useAppSelector} from '../../../../hooks'

import {
    setQuote,
    setCurrencies,
    initWallet,
    setWalletItem,
    getQuote,
    getCurrencies
} from "../../../../features/configurationSlice";
import {CurrencyIcon, getImageSource} from "../../../../currency";
import {getSymbols} from "../../../../market";
import {BinanceSymbol} from "../../../../chart/markets/BinanceMarketData";

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            display: 'flex',
            justifyContent: 'center',
            justifyItems: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            height: '100%'
        },
        "::-webkit-scrollbar": {
            width: 0
        },
        quoteAutoComplete: {
            width: 200
        },
        baseAutoComplete: {
            minWidth: 200,
            maxWidth: '70%',
        },
        item: {
            margin: 20
        },
        chip: {
            marginRight: 5
        },
    }),
);


export default function AssetConfiguration() {
    const classes = useStyles()

    const [symbols, setSymbols] = useState<Array<BinanceSymbol>>([])

    const [bases, setBases] = useState<Array<string>>([])
    const [quotes, setQuotes] = useState<Array<string>>([])
    const [requestCount, setRequestCount] = useState(0)

    const selectedQuote = useAppSelector(getQuote)
    const selectedCurrencies = useAppSelector(getCurrencies)
    const dispatch = useAppDispatch()

    const [loading, setLoading] = useState(true)

    const selectQuote = (quote: string) => {
        dispatch(setCurrencies(selectedCurrencies.filter(c => c !== quote.toUpperCase())))
        dispatch(setQuote(quote || ""))
        dispatch(setWalletItem({currency: quote, quantity: 0}))

        processSymbols(quote)
    }

    const selectBases = useCallback((bases: Array<string>) => {
        if (bases.length < 10) {
            dispatch(setCurrencies(bases))
            dispatch(initWallet(bases))
        }
    }, [dispatch])

    const processSymbols = useCallback((selectedQuote: string) => {
        if(symbols.length) {
            setQuotes([...new Set(symbols.map(s => s.quoteAsset))])
            const bases = [...new Set(symbols.filter(s => s.quoteAsset === selectedQuote).map(s => s.baseAsset))]
            setBases(bases)
            selectBases(selectedCurrencies.filter(b => bases.includes(b)))
        }
    }, [selectBases, selectedCurrencies, symbols])

    useEffect(() => {
        if (!quotes.length && requestCount < 3) {
            getSymbols().then(symbols => {
                setSymbols(symbols)
                setRequestCount(requestCount + 1)
                processSymbols(selectedQuote)
            })
        } else {
            setLoading(false)
        }
    }, [symbols, requestCount, quotes, selectedQuote, processSymbols])

    return (<div className={classes.root}>
        {!quotes.length && !loading &&
        <Alert severity="error">Failed to load market data, check your connection</Alert>}
        {loading && <CircularProgress/>}

        {!loading && quotes.length > 0 &&
        <>
            <Autocomplete
                className={classNames(classes.quoteAutoComplete, classes.item)}
                options={quotes}
                value={selectedQuote}
                disableClearable={true}
                onChange={(_, value) => selectQuote(value)}
                renderOption={(option) => (
                    <>
                        <CurrencyIcon currency={option}/>
                        {option}
                    </>
                )}
                renderInput={(params) =>
                    <TextField
                        {...params}
                        autoComplete="off"
                        InputProps={{
                            ...params.InputProps,
                            startAdornment: (
                                <>
                                    {selectedQuote && <CurrencyIcon currency={selectedQuote}/>}
                                </>
                            )
                        }}
                        label="Quote" variant={"outlined"}/>

                }
            />

            <Autocomplete
                className={classNames(classes.baseAutoComplete, classes.item)}
                multiple
                value={selectedCurrencies}
                autoHighlight={true}
                disableCloseOnSelect
                ListboxComponent={LimitedListComponent as React.ComponentType<React.HTMLAttributes<HTMLElement>>}
                renderGroup={LimitedListRenderGroup}
                onChange={(_, values) => selectBases(values)}
                options={bases.filter(c => c !== selectedQuote)}
                disableClearable={true}
                limitTags={10}
                renderOption={(option) => (
                    <>
                        <CurrencyIcon currency={option}/>
                        {option}
                    </>
                )}
                renderTags={(values, _) => (
                    values.map(value => (
                        <Chip
                            key={value}
                            label={value}
                            className={classes.chip}
                            variant="outlined"
                            avatar={<Avatar src={getImageSource(value)}/>}
                            onDelete={(_) => dispatch(setCurrencies(selectedCurrencies.filter(c => c !== value)))}
                        />
                    ))
                )}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        autoComplete="off"
                        variant={"outlined"}
                        label={"Currencies"}
                        InputProps={{
                            readOnly: selectedCurrencies.length >= 10,
                            ...params.InputProps
                        }}
                    />
                )}
            />
        </>
        }
    </div>)

}