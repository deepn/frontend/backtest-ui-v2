import React from 'react';
import {
  createStyles,
  makeStyles,
  Theme,
} from '@material-ui/core/styles';
import {
  getLongConfiguration,
  getShortConfiguration,
  setLongConfiguration,
  setShortConfiguration,
} from '../../../../features/configurationSlice';
import { NumberInput } from '../../../../inputs/NumberInput';
import {
  useAppDispatch,
  useAppSelector,
} from '../../../../hooks';
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Typography,
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      justifyItems: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      height: '100%',
    },
    group: {
      borderRadius: 5,
      display: 'flex',
      justifyContent: 'center',
      justifyItems: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      padding: 15,
      position: 'relative',
      margin: 10,
      width: '60%',
    },
    groupLine: {
      display: 'flex',
      justifyContent: 'center',
      justifyItems: 'center',
      alignItems: 'center',
      flexDirection: 'row',
    },
    textField: {
      margin: 10,
      minWidth: 250,
    },
    groupLabel: {
      position: 'absolute',
      left: 20,
      top: 0,
      transform: 'translateY(-50%)',
      backgroundColor: 'white',
      paddingLeft: 5,
      paddingRight: 5,
    },
    formControl: {
      margin: 10,
      minWidth: 250,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    title: {
      color: '#6B7789'
    }
  }),
);

export default function OrderConfiguration() {
  const classes = useStyles();

  const longConfiguration = useAppSelector(getLongConfiguration);
  const shortConfiguration = useAppSelector(getShortConfiguration);

  const dispatch = useAppDispatch();

  return (
    <div className={classes.root}>
      <div className={classes.group}>
        <Typography variant={'h6'} align={'center'} className={classes.title}>Buy configuration</Typography>
        <div className={classes.groupLine}>
          <NumberInput value={longConfiguration.takeProfitPercent}
                       label="Default TP %"
                       isPercentage
                       onChange={v => dispatch(setLongConfiguration({
                         ...longConfiguration,
                         takeProfitPercent: v,
                       }))} />
          <NumberInput value={longConfiguration.stopLossPercent}
                       label="Default SL %"
                       isPercentage
                       onChange={v => dispatch(setLongConfiguration({
                         ...longConfiguration,
                         stopLossPercent: v,
                       }))} />
        </div>
        <div className={classes.groupLine}>
          <FormControl variant="outlined"
                       className={classes.formControl}>
            <InputLabel id="order-buy-long">Order type</InputLabel>
            <Select
              labelId="order-buy-long"
              label="Order type"
              value={longConfiguration.orderType}
              onChange={e => dispatch(setLongConfiguration({
                ...longConfiguration,
                orderType: e.target.value as string,
                limitOffset: (e.target.value === 'LIMIT' ? longConfiguration.limitOffset : 0),
              }))}
            >
              <MenuItem value={undefined}>
                <em>Script default</em>
              </MenuItem>
              <MenuItem value={'MARKET'}>MARKET</MenuItem>
              <MenuItem value={'LIMIT'}>LIMIT</MenuItem>
            </Select>
          </FormControl>
          <NumberInput disabled={longConfiguration.orderType !== 'LIMIT'}
                       value={longConfiguration.limitOffset}
                       label="Limit offset %"
                       isPercentage
                       onChange={v => dispatch(setLongConfiguration({
                         ...longConfiguration,
                         limitOffset: v,
                       }))} />
        </div>
      </div>
      <Typography variant={'h6'} align={'center'} className={classes.title}>Sell configuration</Typography>
      <div className={classes.groupLine}>
        <FormControl variant="outlined"
                     className={classes.formControl}>
          <InputLabel id="order-buy-short">Order type</InputLabel>
          <Select
            labelId="order-buy-short"
            label="Order type"
            value={shortConfiguration.orderType}
            onChange={e => dispatch(setShortConfiguration({
              ...shortConfiguration,
              orderType: e.target.value as string,
              limitOffset: (e.target.value === 'LIMIT' ? shortConfiguration.limitOffset : 0),
            }))}
          >
            <MenuItem value={undefined}>
              <em>Script default</em>
            </MenuItem>
            <MenuItem value={'MARKET'}>MARKET</MenuItem>
            <MenuItem value={'LIMIT'}>LIMIT</MenuItem>
          </Select>
        </FormControl>
        <NumberInput disabled={shortConfiguration.orderType !== 'LIMIT'}
                     value={shortConfiguration.limitOffset}
                     label="Limit offset %"
                     isPercentage
                     onChange={v => dispatch(setShortConfiguration({
                       ...shortConfiguration,
                       limitOffset: v,
                     }))} />
      </div>

    </div>
  );
}
