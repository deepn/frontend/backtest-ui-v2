import React, {useEffect, useState} from 'react'
import {createStyles, makeStyles} from "@material-ui/core/styles";
import {CircularProgress, Typography} from "@material-ui/core";
import {CryptoWalletItem, CurrencyIcon} from "../../../../currency";
import {useAppDispatch, useAppSelector} from "../../../../hooks";
import {Cell, Pie, PieChart, ResponsiveContainer} from "recharts";
import {getImageColor, ImageColorResult} from "../../../../utils";
import {extractBase, getPrices} from "../../../../market";
import {Ticker} from "ccxt";
import classNames from "classnames";
import {getCurrencies, getQuote, setWalletItem} from "../../../../features/configurationSlice";

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            display: 'flex',
            justifyItems: 'center',
            alignItems: 'center',
            flexDirection: 'row',
            flexWrap: 'wrap',
            height: '100%',
            marginTop: 10
        },
        container: {
            overflow: 'auto',
            height: 'inherit',
            display: 'flex',
            flex: 2,
            flexDirection: 'row',
            flexWrap: 'wrap',
            alignItems: 'center',
            position: 'relative',
            justifyContent: 'center',
            minWidth: 200,
            alignContent: 'center'
        },
        wallet: {
            flex: 3
        },
        centeredText: {
            display: 'flex',
            alignItems: 'center',
            position: 'absolute',
            left: '50%',
            top: '50%',
            transform: 'translate(-48%, -52%)',
            zIndex: 100
        },
        marginRight: {
            marginRight: 5
        }
    })
);

export default function WalletConfiguration() {
    const classes = useStyles()

    const selectedQuote = useAppSelector(getQuote)
    const selectedCurrencies = useAppSelector(getCurrencies)
    const wallet = useAppSelector((state) => Object.values(state.configuration.value.configuration.wallet)
        .filter(w => selectedCurrencies.includes(w.currency.toUpperCase()) || w.currency === selectedQuote))
    const dispatch = useAppDispatch()

    const [coinColors, setCoinColors] = useState<Array<ImageColorResult>>([])
    const [prices, setPrices] = useState<Array<Ticker>>([])

    useEffect(() => {
        Promise.all(wallet.map(w => getImageColor(document.getElementById("wallet_" + w.currency) as HTMLImageElement))).then(colors => {
            setCoinColors(colors)
        })
    }, [wallet])

    useEffect(() => {
        if (!prices.length)
            getPrices(selectedCurrencies, selectedQuote).then(prices => setPrices(prices))
    }, [selectedCurrencies, selectedQuote, prices])

    const getPrice = (symbol: string, quantity: number) => {
        if (symbol === selectedQuote) return quantity
        return (prices.find(ticker => extractBase(ticker.symbol) === symbol.toUpperCase())?.last || 0) * quantity
    }

    const getTotalWalletPrice = () => wallet.reduce((acc, w) => acc + getPrice(w.currency.toUpperCase(), w.quantity), 0)

    return (
        <div className={classes.root}>
            <div className={classNames(classes.container, classes.wallet)}>
                {[selectedQuote, ...selectedCurrencies].map(currency => (
                    <CryptoWalletItem key={currency}
                                      data={wallet.find(c => c.currency.toUpperCase() === currency.toUpperCase())}
                                      currency={currency}
                                      onChange={walletCurrency => dispatch(setWalletItem(walletCurrency))}/>
                ))}
            </div>

            <div className={classes.container}>
                <div className={classes.centeredText}>
                    {prices.length > 0 && <>
                        <Typography variant={'h5'}
                                    className={classes.marginRight}>{getTotalWalletPrice().toLocaleString()}</Typography>
                        <CurrencyIcon currency={selectedQuote} height={22} width={22}/>
                    </>}
                    {(!prices.length) && <CircularProgress/>}
                </div>
                {coinColors && coinColors.length && <ResponsiveContainer width="98%" height="98%">
                    <PieChart>
                        <Pie
                            data={wallet}
                            nameKey="currency"
                            dataKey={(value) => wallet.every(w => !w.quantity) ? 1 : getPrice(value.currency, value.quantity)}
                            innerRadius="60%"
                            outerRadius="80%"
                            paddingAngle={3}
                            fill="#8884d8"
                        >
                            {wallet.map((entry, _) => (
                                <Cell key={`cell-${entry.currency}`}
                                      fill={coinColors.find(c => c.currency === entry.currency)?.color || "#000000"}/>
                            ))}
                        </Pie>
                    </PieChart>

                </ResponsiveContainer>}
            </div>


        </div>
    )
}
