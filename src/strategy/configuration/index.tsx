import React, { useState } from 'react';
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
} from '@material-ui/core/styles';
import clsx from 'clsx';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Check from '@material-ui/icons/Check';
import StepConnector from '@material-ui/core/StepConnector';
import { StepIconProps } from '@material-ui/core/StepIcon';
import {
  ChevronLeft,
  ChevronRight,
  FiberManualRecord,
  Refresh,
} from '@material-ui/icons';
import AssetConfiguration from './components/assets';
import WalletConfiguration from './components/wallet';
import PeriodSelector from './components/period';
import classNames from 'classnames';
import TradingConfiguration from './components/trading';
import { useAppSelector } from '../../hooks';
import {
  getCurrencies,
  getFeesSchedule,
  getQuote,
  getTradeConfiguration,
} from '../../features/configurationSlice';
import { validateNumbers } from '../../utils';
import OrderConfiguration from './components/order';

const StepIcon = withStyles({
  vertical: {
    padding: 0,
    margin: 0,
  },
  active: {
    '& $line': {
      borderColor: '#BA8CFF',
    },
  },
  completed: {
    '& $line': {
      borderColor: '#8C40FF',
    },
  },
  line: {
    borderColor: '#eaeaf0',
    borderLeftWidth: 3,
    borderRadius: 1,
    height: '100%',
  },
})(StepConnector);

const useStepIconStyle = makeStyles({
  root: {},
  active: {},
  circle: {
    fontSize: 'small',
    color: '#BA8CFF',
  },
  completed: {
    fontSize: 'small',
    color: '#8C40FF',
  },
});

function StyledStepIcon(props: StepIconProps) {
  const classes = useStepIconStyle();
  const { active, completed } = props;

  return (
    <div className={clsx(classes.root, { [classes.active]: active })}>
      {completed ? <Check className={classes.completed} /> :
        <FiberManualRecord className={classes.circle} />}
    </div>
  );
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      height: '100%',
    },
    button: {
      position: 'absolute',
      bottom: 10,
      minWidth: 0,
      borderRadius: 100,
      height: 30,
      width: 30,
      padding: 5,
      color: 'white',
      backgroundColor: theme.palette.primary.main,
      '&:hover': {
        backgroundColor: theme.palette.primary.light,
      },
    },
    right: {
      right: 30,
    },
    left: {
      right: 80,
    },
    disabled: {
      backgroundColor: 'grey',
      '&:hover': {
        backgroundColor: 'grey',
      },
    },
    stepper: {
      display: 'flex',
      alignItems: 'center',
      padding: 0,
      paddingLeft: 10,
      minWidth: 20,
      width: 20,
    },
    content: {
      height: '100%',
      width: 'calc(100% - 26px)',
    },
    iconContainer: {
      margin: 0,
      padding: 0,
    },
    panel: {
      display: 'flex',
      height: '100%',
      width: 'calc(100% - 26px)',
    },
  }),
);

function getSteps() {
  return ['Currencies', 'Wallet', 'Period', 'Trading', 'Order'];
}

function getStepContent(step: number) {
  switch (step) {
    case 0:
      return <AssetConfiguration />;
    case 1:
      return <WalletConfiguration />;
    case 2:
      return <PeriodSelector />;
    case 3:
      return <TradingConfiguration />;
    case 4:
      return <OrderConfiguration />;
    default:
      return 'Error';
  }
}

export default function StrategyConfiguration() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();

  const selectedQuote = useAppSelector(getQuote);
  const selectedCurrencies = useAppSelector(getCurrencies);

  const feesSchedule = useAppSelector(getFeesSchedule);
  const tradeConfiguration = useAppSelector(getTradeConfiguration);


  const wallet = useAppSelector((state) => Object.values(state.configuration.value.configuration.wallet)
    .filter(w => selectedCurrencies.includes(w.currency.toUpperCase()) || w.currency === selectedQuote));

  const handleNext = () => {
    setActiveStep((prevActiveStep) => Math.min(prevActiveStep + 1, steps.length - 1));
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => Math.max(prevActiveStep - 1, 0));
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const validate = (): boolean => {
    switch (activeStep) {
      case 0:
        return selectedQuote.length > 0 && selectedCurrencies.length > 0;
      case 1 :
        return wallet.some(w => w.quantity > 0);
      case 2:
        return true;
      case 3 :
        return validateNumbers(
            [feesSchedule.maker, feesSchedule.taker,
              tradeConfiguration.maximumQuantity,
              tradeConfiguration.minimumQuantity,
            ]) && tradeConfiguration.minimumQuantity <= tradeConfiguration.maximumQuantity
          && tradeConfiguration.minimumQuantity > 0;
    }
    return false;
  };

  return (
    <div className={classes.root}>
      <div className={classes.panel}>
        <Stepper className={classes.stepper}
                 orientation="vertical"
                 activeStep={activeStep}
                 connector={<StepIcon />}>
          {steps.map((label, index) => (
            <Step key={label}
                  onClick={() => setActiveStep(index)}>
              <StepLabel classes={{ iconContainer: classes.iconContainer }}
                         StepIconComponent={StyledStepIcon} />
            </Step>
          ))}
        </Stepper>
        <div className={classes.content}>
          {getStepContent(activeStep)}
          {activeStep > 0 && <ChevronLeft
              onClick={handleBack}
              className={classNames(classes.button, classes.left)}
          />}
          {activeStep < steps.length - 1 && <ChevronRight
              onClick={(_) => validate() && handleNext()}
              className={clsx(classes.button, classes.right, { [classes.disabled]: !validate() })}
          />}
          {activeStep === steps.length - 1 && <Refresh
              onClick={handleReset}
              className={classNames(classes.button, classes.right)}
          />}
        </div>
      </div>
    </div>
  );
}
