import React from 'react';
import {makeStyles, Theme} from '@material-ui/core/styles';
import {StyledTab, StyledTabs, TabIcon, TabPanel} from "../tabs";
import DeepScriptEditor from "./editor";
import StrategyConfiguration from "./configuration";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        height: 'calc(100% - 50px)',
        backgroundColor: theme.palette.background.paper,
    },
    panel: {
        height: '100%',
    }
}));

export default function ConfigurationTabs() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <StyledTabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
                variant="scrollable"
                scrollButtons="auto"
            >
                <StyledTab label="DeepScript" icon={<TabIcon source={'icons/code.svg'}/>}/>
                <StyledTab label="Configuration" icon={<TabIcon source={'icons/settings.svg'}/>}/>
            </StyledTabs>
            <TabPanel value={value} index={0} className={classes.panel}>
                <DeepScriptEditor/>
            </TabPanel>
            <TabPanel value={value} index={1} className={classes.panel}>
                <StrategyConfiguration/>
            </TabPanel>
        </div>
    );
}