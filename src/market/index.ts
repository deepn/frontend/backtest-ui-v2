import ccxt, {Dictionary, Ticker} from "ccxt";
import {BinanceExchangeInfo, BinanceSymbol} from "../chart/markets/BinanceMarketData";

const binance = new ccxt.binance()

const EXCLUDE_SPECIAL_COINS = ((symbol: BinanceSymbol) => ["BEAR", "BULL", "UP", "DOWN"].every(word => !symbol.symbol.toUpperCase().includes(word)))

let symbols: Array<BinanceSymbol> = []

const loadSymbols = (): Promise<Array<BinanceSymbol>> => {
    return new Promise((resolve) => {
        binance.publicGetExchangeInfo().then((info: BinanceExchangeInfo) => {
            resolve(info.symbols.filter(EXCLUDE_SPECIAL_COINS))
        }).catch((_: Error) => resolve([]))
    })

}

const doRequests = async () => {
    if (!symbols.length)
        symbols = await loadSymbols()
}

export const getSymbols = async () => {
    await doRequests()
    return symbols
}

export const getPrices = async (bases: Array<string>, quote: string) => {
    let tickers: Dictionary<Ticker> = await binance.fetchTickers()
    return Object.values(tickers).filter(ticker => bases.map(base => `${base.toUpperCase()}/${quote.toUpperCase()}`).some(symbol => symbol.includes(ticker.symbol)))
}

export const extractBase = (symbol: string) => symbol.split("/")[0].toUpperCase()