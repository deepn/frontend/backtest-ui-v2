import React from "react";
import {Theme, withStyles} from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import {createStyles, Tab} from "@material-ui/core";

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
    className?: string
}

interface StyledTabProps {
    label: string;
    icon?: any
}

interface StyledIconProps {
    source: string;
    classes: any;
}

export const StyledTabs = withStyles({
    root: {
        borderBottom: '1px solid #e8e8e8',
        minHeight: 0,
        height: 38
    },
    indicator: {
        backgroundColor: '#8C40FF',
    }
})((props: any) => <Tabs {...props}/>);

export const TabIcon = withStyles(() =>
    createStyles({
        img: {
            width: 22,
            height: 22,
            marginRight: 10,
            alignItems: 'center'
        }
    })
)((props: StyledIconProps) => <img src={props.source} alt={props.source} className={props.classes.img}/>)

export const TabPanel = (props: TabPanelProps) => {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                children
            )}
        </div>
    );
}


export const StyledTab = withStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingTop: 5,
            paddingBottom: 5,
            minHeight: 0,
            textTransform: 'none',
            fontWeight: theme.typography.fontWeightRegular,
            fontSize: theme.typography.pxToRem(13),
            marginRight: theme.spacing(0),
            '&:hover': {
                opacity: 1,
                color: '#8C40FF'
            },
            '&$selected': {
                color: '#8C40FF'
            },
        },
        wrapper: {
            flexDirection: 'row',
            alignItems: 'end'
        },
        selected: {}
    }),
)((props: StyledTabProps) => <Tab disableRipple {...props} />);