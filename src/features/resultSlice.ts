import {
  createSlice,
  PayloadAction,
} from '@reduxjs/toolkit';
import type { RootState } from '../store';
import {
  FeesSchedule,
  WalletCurrency,
} from './configurationSlice';

export interface Log {
  time: number,
  message: string
}

export interface Order {
  currencyPair: Array<string>,
  id: string,
  fees: number,
  price: number,
  quantity: number,
  side: string,
  status: string,
  time: number,
  type: string
}

export interface Trade {
  currencyPair: Array<string>,
  openTime: number,
  closeTime: number,
  entryPrice: number,
  exitPrice: number,
  fees: number,
  pnl: number,
  quantity: number,
  status: string,
  orders: Array<Order>
}

export interface Market {
  currencyPair: Array<string>,
  marketPrice: number
}

export interface BacktestConfiguration {
  startTime: number,
  endTime: number,
  feesSchedule: FeesSchedule,
  wallet: Array<WalletCurrency>
}

export interface BacktestResultWallet {
  currencies: { [key: string]: Array<WalletCurrency>; },
  trades: Array<Trade>,
  walletValue: Array<number>
}

export interface BacktestResult {
  backtestConfiguration: BacktestConfiguration,
  logs: { [key: string]: Array<Log>; },
  markets: Array<Market>,
  wallet: BacktestResultWallet
}

interface BacktestResultState {
  value: BacktestResult | undefined;
  errors: any
}

const initialState: BacktestResultState = {
  value: undefined,
  errors: undefined
};

export const BacktestResultSlice = createSlice({
  name: 'backtestResult',
  initialState,
  reducers: {
    setBacktestResult: (state, action: PayloadAction<BacktestResult>) => {
      state.value = action.payload;
    },
    setBacktestResultError: (state, action: PayloadAction<any>) => {
      state.errors = action.payload;
    },
  },
});

export const {
  setBacktestResult,
  setBacktestResultError
} = BacktestResultSlice.actions;

export const backtestResult = (state: RootState) => state.backtestResult.value;

export const getTrades = (state: RootState) => state.backtestResult.value?.wallet.trades;
export const getMarkets = (state: RootState) => state.backtestResult.value?.markets;
export const getWallet = (state: RootState) => state.backtestResult.value?.wallet;
export const getLogs = (state: RootState) => state.backtestResult.value?.logs;
export const getErrors = (state: RootState) => state.backtestResult.errors;

export default BacktestResultSlice.reducer;
