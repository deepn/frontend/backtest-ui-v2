import {
  createSlice,
  PayloadAction,
} from '@reduxjs/toolkit';
import type { RootState } from '../store';
import { addDays } from 'date-fns';

export interface Period {
  startTime: number,
  endTime: number,
}

export interface FeesSchedule {
  taker: number,
  maker: number
}

export interface WalletCurrency {
  currency: string | String,
  quantity: number,
  locked?: number | undefined
}

interface TradeOperationConfiguration {
  stopLossPercent?: number,
  takeProfitPercent?: number,
  orderType?: string | String,
  limitOffset?: number | undefined
}

interface TradeConfiguration {
  minimumQuantity: number,
  maximumQuantity: number,
  maximumOpenOrders?: number,
  maximumOrderPerCoin?: number,
  amendStakeAmount: boolean
}

interface TradingConfiguration {
  quoteCurrency: string
  currencies: Array<string>,
  timeFrame: string,
  longConfiguration: TradeOperationConfiguration,
  shortConfiguration: TradeOperationConfiguration,
  tradeConfiguration: TradeConfiguration
}

interface BacktestConfiguration {
  startTime: number,
  endTime: number,
  wallet: Array<WalletCurrency>,
  feesSchedule: FeesSchedule
}

export interface Configuration {
  configuration: BacktestConfiguration,
  tradingConfiguration: TradingConfiguration,
  strategy: string
}

interface ConfigurationState {
  value: Configuration;
}

const initialState: ConfigurationState = {
  value: {
    configuration: {
      startTime: new Date().getTime(),
      endTime: addDays(new Date(), -7).getTime(),
      wallet: [],
      feesSchedule: { taker: 0, maker: 0 },
    },
    tradingConfiguration: {
      quoteCurrency: '',
      currencies: [],
      timeFrame: '5m',
      longConfiguration: {},
      shortConfiguration: {},
      tradeConfiguration: {
        minimumQuantity: 0,
        maximumQuantity: 0,
        amendStakeAmount: true,
      },
    },
    strategy: '',
  },
};

export const ConfigurationSlice = createSlice({
  name: 'configuration',
  initialState,
  reducers: {
    setPeriod: (state, action: PayloadAction<Period>) => {
      state.value.configuration.startTime = action.payload.startTime;
      state.value.configuration.endTime = action.payload.endTime;
    },
    setCurrencies: (state, action: PayloadAction<Array<string>>) => {
      state.value.tradingConfiguration.currencies = action.payload;
    },
    setQuote: (state, action: PayloadAction<string>) => {
      state.value.tradingConfiguration.quoteCurrency = action.payload;
    },
    setTimeFrame: (state, action: PayloadAction<string>) => {
      state.value.tradingConfiguration.timeFrame = action.payload;
    },
    setMakerFees: (state, action: PayloadAction<number>) => {
      state.value.configuration.feesSchedule.maker = action.payload;
    },
    setTakerFees: (state, action: PayloadAction<number>) => {
      state.value.configuration.feesSchedule.taker = action.payload;
    },
    setMinimumQuantity: (state, action: PayloadAction<number>) => {
      state.value.tradingConfiguration.tradeConfiguration.minimumQuantity = action.payload;
    },
    setMaximumQuantity: (state, action: PayloadAction<number>) => {
      state.value.tradingConfiguration.tradeConfiguration.maximumQuantity = action.payload;
    },
    setMaximumOpenOrder: (state, action: PayloadAction<number>) => {
      state.value.tradingConfiguration.tradeConfiguration.maximumOpenOrders = action.payload;
    },
    setMaximumOpenOrderPerCoin: (state, action: PayloadAction<number>) => {
      state.value.tradingConfiguration.tradeConfiguration.maximumOrderPerCoin = action.payload;
    },
    setWalletItem: (state, action: PayloadAction<WalletCurrency>) => {
      const currency = action.payload.currency.toUpperCase()
      const wallet = state.value.configuration.wallet
      state.value.configuration.wallet = wallet.filter((wallet : WalletCurrency) => wallet.currency !== currency)
      state.value.configuration.wallet.push(action.payload);
    },
    initWallet: (state, action: PayloadAction<Array<string>>) => {
      const wallet = state.value.configuration.wallet

      action.payload.forEach(currency => {
        if (!wallet.some((wallet : WalletCurrency) => wallet.currency === currency))
          state.value.configuration.wallet.push({ currency, quantity: 0 });
      });
    },
    setStrategy: (state, action: PayloadAction<string>) => {
      state.value.strategy = action.payload;
    },
    setLongConfiguration: (state, action: PayloadAction<TradeOperationConfiguration>) => {
      state.value.tradingConfiguration.longConfiguration = action.payload;
    },
    setShortConfiguration: (state, action: PayloadAction<TradeOperationConfiguration>) => {
      state.value.tradingConfiguration.shortConfiguration = action.payload;
    },
  },
});

export const {
  setPeriod,
  setCurrencies,
  setQuote,
  setTakerFees,
  setMinimumQuantity,
  setMaximumQuantity,
  setMaximumOpenOrder,
  setMaximumOpenOrderPerCoin,
  setMakerFees,
  setTimeFrame,
  setWalletItem,
  initWallet,
  setStrategy,
  setLongConfiguration,
  setShortConfiguration
} = ConfigurationSlice.actions;

export const getConfigurationValue = (state: RootState) => state.configuration.value;

export const getConfiguration = (state: RootState) => state.configuration.value.configuration;

const tradingConfiguration = (state: RootState) => state.configuration.value.tradingConfiguration;

export const getPeriod = (state: RootState) => {
  const { startTime, endTime } = getConfiguration(state);
  return { startTime, endTime };
};

export const getTimeFrame = (state: RootState) => tradingConfiguration(state).timeFrame;
export const getQuote = (state: RootState) => tradingConfiguration(state).quoteCurrency;
export const getCurrencies = (state: RootState) => tradingConfiguration(state).currencies;
export const getTradeConfiguration = (state: RootState) => tradingConfiguration(state).tradeConfiguration;
export const getFeesSchedule = (state: RootState) => getConfiguration(state).feesSchedule;
export const getStrategy = (state: RootState) => state.configuration.value.strategy;
export const getLongConfiguration = (state: RootState) => tradingConfiguration(state).longConfiguration;
export const getShortConfiguration = (state: RootState) => tradingConfiguration(state).shortConfiguration;

export default ConfigurationSlice.reducer;
