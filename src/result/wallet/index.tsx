import React, { useMemo } from 'react';
import { useAppSelector } from '../../hooks';
import { getWallet } from '../../features/resultSlice';
import {
  Area,
  AreaChart,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import {
  getPeriod,
  getQuote,
} from '../../features/configurationSlice';
import moment from 'moment';


export default function Wallet() {
  const wallet = useAppSelector(getWallet);
  const selectedQuote = useAppSelector(getQuote);
  const period = useAppSelector(getPeriod);


  const currencies = useMemo<Array<any>>(() => {
    if (!wallet) return [];
    const initialValue = wallet.walletValue[0];
    return wallet.walletValue.map((value, index) => ({
      [selectedQuote]: +(value - initialValue).toFixed(2),
      time: moment(period.startTime).add(index, 'hour').format('DD/MM HH:mm'),
    }));
  }, [wallet, period.startTime, selectedQuote]);

  const gradientOffset = () => {
    const dataMax = Math.max(...currencies.map((i) => i[selectedQuote] as number));
    const dataMin = Math.min(...currencies.map((i) => i[selectedQuote] as number));
    if (dataMax <= 0) {
      return 0;
    } else if (dataMin >= 0) {
      return 1;
    } else {
      return dataMax / (dataMax - dataMin);
    }
  };

  const offset = gradientOffset();
  return <>
    {currencies && wallet &&
    <ResponsiveContainer width={'90%'}>
        <AreaChart
            data={currencies}>
            <defs>
                <linearGradient id="splitColor"
                                x1="0"
                                y1="0"
                                x2="0"
                                y2="1">
                    <stop offset={offset}
                          stopColor="green"
                          stopOpacity={1} />
                    <stop offset={offset}
                          stopColor="red"
                          stopOpacity={1} />
                </linearGradient>
            </defs>
            <XAxis dataKey={'time'}
                   interval={Math.ceil(currencies.length / 5)} />
            <YAxis type={'number'}
                   domain={['auto', 'auto']}
                   tickFormatter={(v: any) => v + wallet.walletValue[0]} />
            <Area type="linear"
                  dataKey={selectedQuote}
                  strokeWidth={0}
                  fill="url(#splitColor)"
                  dot={false} />
            <Tooltip formatter={(v: any) => (v + wallet.walletValue[0])} />
            <Legend verticalAlign={'top'}
                    align={'right'} />
        </AreaChart>
    </ResponsiveContainer>
    }
  </>;
}
