import React from 'react';
import {
  makeStyles,
  Theme,
} from '@material-ui/core/styles';
import {
  StyledTab,
  StyledTabs,
  TabIcon,
  TabPanel,
} from '../tabs';
import Trades from './trades';
import Terminal from './terminal';
import Orders from './orders';
import Wallet from './wallet';


const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    height: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  tab: {
    width: '100%',
    height: 'calc(100% - 40px)',
  },
}));

export default function ResultTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <StyledTabs
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
        aria-label="scrollable auto tabs example"
      >
        <StyledTab label="Terminal"
                   icon={<TabIcon source={'icons/terminal.svg'} />} />
        <StyledTab label="Trades"
                   icon={<TabIcon source={'icons/trade.svg'} />} />
        <StyledTab label="Orders"
                   icon={<TabIcon source={'icons/orders.svg'} />} />
        <StyledTab label="Wallet"
                   icon={<TabIcon source={'icons/wallet.svg'} />} />
        <StyledTab label="Statistics"
                   icon={<TabIcon source={'icons/stats.svg'} />} />
      </StyledTabs>
      <TabPanel value={value}
                className={classes.tab}
                index={0}>
        <Terminal />
      </TabPanel>
      <TabPanel value={value}
                className={classes.tab}
                index={1}>
        <Trades />
      </TabPanel>
      <TabPanel value={value}
                className={classes.tab}
                index={2}>
        <Orders />
      </TabPanel>
      <TabPanel value={value}
                className={classes.tab}
                index={3}>
        <Wallet />
      </TabPanel>
      <TabPanel value={value}
                className={classes.tab}
                index={4}>
      </TabPanel>
    </div>
  );
}
