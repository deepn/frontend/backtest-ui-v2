import React, { useMemo } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useAppSelector } from '../../hooks';
import {
  getTrades,
  Order,
} from '../../features/resultSlice';
import moment from 'moment';
import { TablePagination } from '@material-ui/core';
import TablePaginationActions from '@material-ui/core/TablePagination/TablePaginationActions';
import { CurrencyIcon } from '../../currency';


const orderStatusColor = (status: string) => {
  switch (status.toUpperCase()) {
    case "FILLED": return "green"
    case "CANCELED": return "red"
    case "PENDING": return "grey"
    case "PARTIALLY_FILLED": return "orange"
  }
}

export default function Orders() {
  const trades = useAppSelector(getTrades);

  const orders = useMemo<Array<Order>>(() => {
    if(trades) {
      return trades.flatMap(trade => trade.orders).sort((a, b) => a.time - b.time)
    } else return []
  }, [trades])


  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return trades ? (
    <Paper>
      <TableContainer>
        <Table aria-label="orders">
          <TableHead>
            <TableRow>
              <TableCell align={'center'}>Symbol</TableCell>
              <TableCell align={'center'}>Side</TableCell>
              <TableCell align={'center'}>Type</TableCell>
              <TableCell align={'center'}>Time</TableCell>
              <TableCell align={'center'}>Price</TableCell>
              <TableCell align={'center'}>Quantity</TableCell>
              <TableCell align={'center'}>Fees</TableCell>
              <TableCell align={'center'}>Status</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {orders.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(order => (
              <TableRow key={order.currencyPair[0] + order.time}>
                <TableCell align={'center'}>
                  <CurrencyIcon currency={order.currencyPair[0]}
                                width={30}
                                height={30} />
                </TableCell>
                <TableCell align={'center'}><span style={{
                  padding: 6, borderRadius: 10,
                  backgroundColor: order.side === 'BUY' ? 'green' : 'red',
                  color: 'white',
                }}>{order.side}</span></TableCell>
                <TableCell align={'center'}>{order.type}</TableCell>
                <TableCell align={'center'}>{moment(order.time).format('DD/MM/YY HH:mm')}</TableCell>
                <TableCell align={'center'}>{order.price.toFixed(2)}</TableCell>
                <TableCell align={'center'}>{order.quantity.toFixed(2)}</TableCell>
                <TableCell align={'center'}>{order.fees.toFixed(2)}</TableCell>
                <TableCell align={'center'}><span style={{
                  padding: 6, borderRadius: 10,
                  backgroundColor: orderStatusColor(order.status),
                  color: 'white',
                }}>{order.status}</span></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
        colSpan={3}
        count={orders.length}
        rowsPerPage={rowsPerPage}
        page={page}
        SelectProps={{
          inputProps: { 'aria-label': 'rows per page' },
          native: true,
        }}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        ActionsComponent={TablePaginationActions}
      />
    </Paper>
  ) : <></>;
}
