import React, {
  useEffect,
  useState,
} from 'react';
import {
  createStyles,
  makeStyles,
  Theme,
} from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {
  Divider,
  FormControl,
  InputLabel,
  List,
  MenuItem,
  Select,
} from '@material-ui/core';
import { useAppSelector } from '../../hooks';
import {
  getLogs,
  Log,
} from '../../features/resultSlice';
import moment from 'moment';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
  }),
);

export default function Terminal() {
  const classes = useStyles();
  const logs = useAppSelector(getLogs);

  const [selectedKey, setSelectedKey] = useState<string>(Object.keys(logs || {})[0]);

  useEffect(() => {
    if (logs)
      setSelectedKey(Object.keys(logs)[0]);
  }, [logs, setSelectedKey]);

  return logs ? (
    <div className={classes.root}>
      <FormControl style={{ alignSelf: 'center', marginTop: 10 }}>
        <InputLabel id="select-market">Market</InputLabel>
        <Select
          labelId="select-market"
          id="select"
          value={selectedKey || Object.keys(logs)[0] as string}
          onChange={e => setSelectedKey((e.target.value || Object.keys(logs)[0]) as string)}
        >
          {Object.keys(logs).map(key => (
            <MenuItem key={key}
                      value={key}>{key}</MenuItem>
          ))}
        </Select>
      </FormControl>
      <List>
        {logs && logs[selectedKey] && logs[selectedKey].map((log: Log) => (
          <>
            <ListItem>
              <ListItemText key={log.time}
                            primary={moment(log.time).format('DD/MM/YY HH:mm:ss') + ' > ' + log.message} />
            </ListItem>
            <Divider />
          </>
        ))}
      </List>
    </div>
  ) : <></>;
}
