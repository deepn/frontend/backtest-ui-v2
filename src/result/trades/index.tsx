import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useAppSelector } from '../../hooks';
import {
  getTrades,
  Trade,
} from '../../features/resultSlice';
import moment from 'moment';
import { CurrencyIcon } from '../../currency';
import { TablePagination } from '@material-ui/core';
import TablePaginationActions from '@material-ui/core/TablePagination/TablePaginationActions';


const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

const orderStatusColor = (status: string) => {
  switch (status.toUpperCase()) {
    case 'FILLED':
      return 'green';
    case 'CANCELED':
      return 'red';
    case 'PENDING':
      return 'grey';
    case 'PARTIALLY_FILLED':
      return 'orange';
  }
};

function Row(props: { row: Trade }) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}
                onClick={() => setOpen(!open)}>
        <TableCell align={'center'}>
          <CurrencyIcon currency={row.currencyPair[0]}
                        width={30}
                        height={30} />
        </TableCell>
        <TableCell align={'center'}>{moment(row.openTime).format('DD/MM/YY HH:mm:ss')}</TableCell>
        <TableCell align={'center'}>{row.closeTime ? moment.duration(row.closeTime - row.openTime, 'milliseconds').humanize() : '-'}</TableCell>
        <TableCell align={'center'}>{row.quantity.toFixed(2)}</TableCell>
        <TableCell align={'center'}>{row.entryPrice.toFixed(2)}</TableCell>
        <TableCell align={'center'}>{row.exitPrice === 0 ? '-' : row.exitPrice.toFixed(2)}</TableCell>
        <TableCell align={'center'} style={{ color: row.pnl > 0 ? 'green' : row.pnl === 0 ? 'grey' : 'red' }}>
          <b>{row.exitPrice === 0 ? '-' : row.pnl.toFixed(2)}</b>
        </TableCell>
        <TableCell align={'center'}>{row.exitPrice === 0 ? '-' : row.fees.toFixed(2)}</TableCell>
        <TableCell align={'center'}><span style={{
          padding: 6, borderRadius: 10,
          backgroundColor: row.status === 'CLOSED' ? 'green' : 'grey',
          color: 'white',
        }}>{row.status}</span></TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }}
                   colSpan={9}>
          <Collapse in={open}
                    timeout="auto"
                    unmountOnExit>
            <Box>
              <Table size="small"
                     aria-label="orders">
                <TableHead>
                  <TableRow>
                    <TableCell align={'center'}>Side</TableCell>
                    <TableCell align={'center'}>Type</TableCell>
                    <TableCell align={'center'}>Time</TableCell>
                    <TableCell align={'center'}>Price</TableCell>
                    <TableCell align={'center'}>Quantity</TableCell>
                    <TableCell align={'center'}>Fees</TableCell>
                    <TableCell align={'center'}>Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.orders.map(order => (
                    <TableRow key={order.time}>
                      <TableCell align={'center'}><span style={{
                        padding: 6, borderRadius: 10,
                        backgroundColor: order.side === 'BUY' ? 'green' : 'red',
                        color: 'white',
                      }}>{order.side}</span></TableCell>
                      <TableCell align={'center'}>{order.type}</TableCell>
                      <TableCell align={'center'}>{moment(order.time).format('DD/MM/YY HH:mm:ss')}</TableCell>
                      <TableCell align={'center'}>{order.price.toFixed(2)}</TableCell>
                      <TableCell align={'center'}>{order.quantity.toFixed(2)}</TableCell>
                      <TableCell align={'center'}>{order.fees.toFixed(2)}</TableCell>
                      <TableCell align={'center'}><span style={{
                        padding: 6, borderRadius: 10,
                        backgroundColor: orderStatusColor(order.status),
                        color: 'white',
                      }}>{order.status}</span></TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

export default function Trades() {
  const trades = useAppSelector(getTrades);

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return trades ? (
    <Paper>
      <TableContainer>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell align={'center'}>Symbol</TableCell>
              <TableCell align={'center'}>Open</TableCell>
              <TableCell align={'center'}>Duration</TableCell>
              <TableCell align={'center'}>Quantity</TableCell>
              <TableCell align={'center'}>Entry</TableCell>
              <TableCell align={'center'}>Exit</TableCell>
              <TableCell align={'center'}>P/L</TableCell>
              <TableCell align={'center'}>Fees</TableCell>
              <TableCell align={'center'}>Status</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {trades && trades.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row: Trade) => (
              <Row key={row.quantity + row.openTime + row.fees}
                   row={row} />
            ))}
          </TableBody>

        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
        colSpan={3}
        count={trades.length}
        rowsPerPage={rowsPerPage}
        page={page}
        SelectProps={{
          inputProps: { 'aria-label': 'rows per page' },
          native: true,
        }}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        ActionsComponent={TablePaginationActions}
      />
    </Paper>
  ) : <></>;
}
