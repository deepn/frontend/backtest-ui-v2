import * as React from 'react';
import {
  useEffect,
  useRef,
  useState,
} from 'react';

import {
  ChartingLibraryWidgetOptions,
  EntityId,
  IChartingLibraryWidget,
  IExecutionLineAdapter,
  widget,
} from '../charting_library';
import {
  BinanceMarketData,
  convertTimeFrameToTV,
} from './markets/BinanceMarketData';
import { useAppSelector } from '../hooks';
import {
  getCurrencies,
  getPeriod,
  getQuote,
  getTimeFrame,
  Period,
} from '../features/configurationSlice';
import { getTrades } from '../features/resultSlice';
import moment from 'moment';

const container = 'tv_chart_container';

const Chart = () => {

  const period = useAppSelector(getPeriod);
  const timeFrame = useAppSelector(getTimeFrame);
  const currencies = useAppSelector(getCurrencies);
  const quote = useAppSelector(getQuote);

  const trades = useAppSelector(getTrades);

  const [currentPeriod, setCurrentPeriod] = useState<Period>(period);
  const [currentTimeFrame, setCurrentTimeFrame] = useState<string>(timeFrame);
  const [currentCurrencies, setCurrentCurrencies] = useState<Array<string>>(currencies);
  const [currentQuote, setCurrentQuote] = useState<string>(quote);
  const [selectedSymbol, setSelectedSymbol] = useState<string>(quote);

  const widgetRef = useRef<IChartingLibraryWidget>();
  const shapes = useRef<Array<IExecutionLineAdapter>>();
  const marketData = useRef<BinanceMarketData>();

  useEffect(() => {
    if (JSON.stringify(currentPeriod) !== JSON.stringify(period)) {
      setCurrentPeriod(period);
    }
    if (timeFrame !== currentTimeFrame) {
      setCurrentTimeFrame(timeFrame);
    }

    if (quote !== currentQuote) {
      setCurrentQuote(quote);
    }

    if (JSON.stringify(currencies) !== JSON.stringify(currentCurrencies)) {
      setCurrentCurrencies(currencies);
    }

  }, [period, timeFrame, currentPeriod, currentTimeFrame, currencies, currentCurrencies, quote, currentQuote]);


  useEffect(() => {
    console.log('Update trades !');

    shapes.current?.forEach(s => s.remove());
    shapes.current = undefined;


    if (widgetRef.current && trades) {
      const chart = widgetRef.current.activeChart();

      if (!chart) return;

      chart.clearMarks();
      marketData.current?.setTrades(trades);
      chart.refreshMarks()

      const currentShapes: Array<IExecutionLineAdapter> = [];
      trades.filter(t => t.currencyPair[0] === chart.symbolExt().full_name.split('/')[0]).forEach(trade => {
        trade.orders.forEach(order => {
          const executionShape = chart.createExecutionShape();
          executionShape.setPrice(order.price - 100);
          executionShape.setDirection(order.side === 'BUY' ? 'buy' : 'sell');
          executionShape.setTooltip(
            'Price: ' + order.type +
            '\nQuantity: ' + order.quantity.toFixed(4) + ' ' + order.currencyPair[0] +
            '\nFees: ' + order.fees.toFixed(4) + ' ' + order.currencyPair[1] +
            '\nType: ' + order.type +
            '\nSide: ' + order.side +
            '\nTime: ' + moment(order.time).format('DD/MM/YY HH:mm'));

          executionShape.setTime(Math.ceil(order.time / 1000));
          executionShape.setArrowHeight(16);
          currentShapes.push(executionShape);
        });

      });
      shapes.current = currentShapes;
    }
  }, [trades, selectedSymbol]);

  useEffect(() => {
    marketData.current = new BinanceMarketData(currentPeriod, currentCurrencies, currentQuote, currentTimeFrame);
    widgetRef.current = new widget({
      symbol: (currentCurrencies[0] + currentQuote).toUpperCase(),
      datafeed: marketData.current,
      interval: convertTimeFrameToTV(currentTimeFrame) as ChartingLibraryWidgetOptions['interval'],
      container: container,
      library_path: '/charting_library/',
      locale: 'en',
      disabled_features: ['timezone_menu'],
      enabled_features: ['study_templates', 'use_localstorage_for_settings'],
      client_id: 'Deepn',
      user_id: 'deepn_id',
      fullscreen: false,
      autosize: true,
      studies_overrides: {},
    });

    widgetRef.current.onChartReady(() => {
      console.log('Chart ready ?');
      let lastId: EntityId | undefined | null = null;

      const update = () => {
        const chart = widgetRef.current?.activeChart();

        if (!chart) return;

        if (lastId != null)
          chart.removeEntity(lastId);

        lastId = chart.createShape({ time: Math.ceil(currentPeriod.startTime / 1000) }, {
          shape: 'vertical_line', text: 'Start of backtest', overrides: {
            linecolor: 'black',
            textcolor: 'black',
            showLabel: true,
            textOrientation: 'horizontal',
            horzLabelsAlign: 'right',
            vertLabelsAlign: 'bottom',
            fontsize: 20,
          },
        });
        setSelectedSymbol(chart.symbol());
      };

      widgetRef.current?.activeChart().onSymbolChanged().subscribe(null, () => {
        update();
      });

      update();
    });


  }, [currentCurrencies, currentPeriod, currentQuote, currentTimeFrame]);

  return (
    <div
      id={container}
      style={{ height: '100%' }}
    />
  );

};

export default Chart;
