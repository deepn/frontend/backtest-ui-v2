import ccxt, { OHLCV } from 'ccxt';
import {
  Bar,
  ErrorCallback,
  GetMarksCallback,
  HistoryCallback,
  IDatafeedChartApi,
  LibrarySymbolInfo,
  Mark,
  OnReadyCallback,
  PeriodParams,
  QuotesCallback,
  ResolutionString,
  ResolveCallback,
  SearchSymbolsCallback,
  SubscribeBarsCallback,
  SymbolResolveExtension,
  TimescaleMark,
} from '../../charting_library/datafeed-api';
import { Period } from '../../features/configurationSlice';
import { Trade } from '../../features/resultSlice';
import moment from 'moment';

const binance = new ccxt.binance();

const RESOLUTIONS_INTERVALS_MAP: { [key: string]: string; } = {
  '1': '1m',
  '3': '3m',
  '5': '5m',
  '15': '15m',
  '30': '30m',
  '60': '1h',
  '120': '2h',
  '240': '4h',
  '360': '6h',
  '480': '8h',
  '720': '12h',
  'D': '1d',
  '1D': '1d',
  '3D': '3d',
  'W': '1w',
  '1W': '1w',
  'M': '1M',
  '1M': '1M',
};

const INTERVAL_TO_MS: { [key: string]: number; } = {
  '1': 60000,
  '3': 60000 * 3,
  '5': 60000 * 5,
  '15': 60000 * 15,
  '30': 60000 * 30,
  '60': 60000 * 60,
  '120': 60000 * 120,
  '240': 60000 * 240,
  '360': 60000 * 360,
  '480': 60000 * 480,
  '720': 60000 * 720,
  'D': 86400 * 1000,
  '1D': 86400 * 1000,
  '3D': 86400 * 1000 * 3,
  'W': 86400 * 1000 * 7,
  '1W': 86400 * 1000 * 7,
  'M': 86400 * 1000 * 30,
  '1M': 86400 * 1000 * 30,
};

export const convertTimeFrameToTV = (timeFrame: string) => {
  const value = [...Object.entries(RESOLUTIONS_INTERVALS_MAP)]
    .filter(({ 1: v }) => v === timeFrame)
    .map(([k]) => k);

  return value.length ? value[0] : '60';
};

export interface BinanceSymbol {
  symbol: string,
  baseAsset: string,
  quoteAsset: string,
  filterType: string,
  tickSize: string,
  filters: Array<{
    filterType: string,
    tickSize: string
  }>
}

export interface BinanceExchangeInfo {
  symbols: Array<BinanceSymbol>;
}

export class BinanceMarketData implements IDatafeedChartApi {
  private readonly symbols: Promise<Array<LibrarySymbolInfo>>;
  private readonly allSymbols: Promise<BinanceExchangeInfo>;

  private period: Period;
  private currencies: Array<string>;
  private readonly quote: string;
  private readonly timeFrame: string;
  private loaded: boolean = false;
  private loadedSymbol: string = '';
  private trades: Array<Trade> = [];

  constructor(period: Period, currencies: Array<string>, quote: string, timeFrame: string) {
    this.period = period;
    this.currencies = currencies;
    this.quote = quote;
    this.timeFrame = timeFrame;
    const { symbols, allSymbols } = this.loadSymbols();
    this.symbols = symbols;
    this.allSymbols = allSymbols;
    console.log('New binance !');
  }

  setTrades = (trades: Array<Trade>) => {
    this.trades = trades;
  };

  loadSymbols() {
    function priceScale(symbol: BinanceSymbol) {
      const filter = symbol.filters.find(filter => filter.filterType === 'PRICE_FILTER');
      return filter ? Math.round(1 / parseFloat(filter.tickSize)) : 1;
    }

    const promise = binance.publicGetExchangeInfo();
    const authorizedSymbols = this.currencies.map(base => `${base}/${this.quote}`);

    return {
      symbols: promise.then((info: BinanceExchangeInfo) => {
        return info.symbols.filter(s => authorizedSymbols.includes(`${s.baseAsset}/${s.quoteAsset}`)).map(symbol => {
          return {
            symbol: symbol.symbol,
            ticker: symbol.symbol,
            name: symbol.symbol,
            full_name: `${symbol.baseAsset}/${symbol.quoteAsset}`,
            description: `${symbol.baseAsset} / ${symbol.quoteAsset}`,
            exchange: 'BINANCE',
            listed_exchange: 'BINANCE',
            type: 'crypto',
            currency_code: symbol.quoteAsset,
            session: '24x7',
            timezone: 'UTC',
            pro_name: `${symbol.baseAsset}/${symbol.quoteAsset}`,
            minmovement: 1,
            minmov: 1,
            minmovement2: 0,
            minmov2: 0,
            pricescale: priceScale(symbol),
            supported_resolutions: [convertTimeFrameToTV(this.timeFrame)],
            has_intraday: true,
            has_daily: true,
            has_weekly_and_monthly: true,
          };
        });
      }), allSymbols: promise.then((info: BinanceExchangeInfo) => {
        let set = new Set();
        for (const symbol of info.symbols)
          if (authorizedSymbols.includes(`${symbol.baseAsset}/${symbol.quoteAsset}`))
            set.add(symbol.symbol);
        return set;
      }),
    };
  }

  onReady = async (callback: OnReadyCallback) => {
    setTimeout(() => callback({ supports_marks: true, supports_timescale_marks: true }),0);
  };

  searchSymbols = async (userInput: string, exchange: string, symbolType: string, onResult: SearchSymbolsCallback) => {
    let symbols = await this.symbols;

    if (symbolType)
      symbols = symbols.filter(s => s.type === symbolType);

    if (exchange)
      symbols = symbols.filter(s => s.exchange === exchange);

    let query = userInput.toUpperCase();
    symbols = symbols.filter(s => s.name.indexOf(query) >= 0);

    onResult(symbols.map((s: LibrarySymbolInfo) => ({
        symbol: s.name,
        full_name: s.full_name,
        description: s.description,
        exchange: s.exchange,
        ticker: s.ticker!,
        type: s.type,
      })),
    );
  };

  resolveSymbol = async (symbolName: string, onResolve: ResolveCallback, onError: ErrorCallback, extension?: SymbolResolveExtension) => {
    const symbols = await this.symbols;

    const comps = symbolName.split(':');
    const s = (comps.length > 1 ? comps[1] : symbolName).toUpperCase();

    for (const symbol of symbols) {
      if (symbol.name === s) {
        onResolve(symbol);
        return;
      }
    }

    onError('error');
  };

  getBars = async (symbolInfo: LibrarySymbolInfo, resolution: ResolutionString, periodParams: PeriodParams, onResult: HistoryCallback, onError: ErrorCallback) => {
    const interval = RESOLUTIONS_INTERVALS_MAP[resolution];

    console.log('Timezone: ', symbolInfo.timezone);
    let from = this.period.startTime - (INTERVAL_TO_MS[resolution] * 200);
    let to = this.period.endTime;
    let ticker = symbolInfo.full_name;

    if (this.loaded && this.loadedSymbol === ticker) {
      onResult([], { noData: true });
      this.loaded = false;
      this.loadedSymbol = '';
      return;
    }


    this.loaded = false;

    return binance.fetchOHLCV(ticker, interval, from, periodParams.countBack + 200, {
      endTime: to,
    }).then(klines => {
      let bars: Array<Bar> = [];
      klines.forEach((b: OHLCV) => {
        bars.push({
          time: b[0],
          open: b[1],
          high: b[2],
          low: b[3],
          close: b[4],
          volume: b[5],
        });
      });
      this.loaded = true;
      this.loadedSymbol = ticker;
      if (bars.length)
        onResult(bars, { noData: false });
      else
        onResult(bars, { noData: true });

    }).catch(e => onError('failed'));
  };

  subscribeBars = (symbolInfo: LibrarySymbolInfo, resolution: ResolutionString, onTick: SubscribeBarsCallback,
                   listenerGuid: string, onResetCacheNeededCallback: () => void): void => {

  };
  unsubscribeBars = (subscriberUID: string) => {
  };

  public getQuotes(symbols: string[], onDataCallback: QuotesCallback, onErrorCallback: (msg: string) => void): void {
  }

  public subscribeQuotes(symbols: string[], fastSymbols: string[], onRealtimeCallback: QuotesCallback, listenerGuid: string): void {
  }

  public unsubscribeQuotes(listenerGuid: string): void {
  }

  public getMarks(symbolInfo: LibrarySymbolInfo, from: number, to: number, onDataCallback: GetMarksCallback<Mark>, resolution: ResolutionString): void {
  }

  getTimescaleMarks = async (symbolInfo: LibrarySymbolInfo, from: number, to: number, onDataCallback: GetMarksCallback<TimescaleMark>, resolution: ResolutionString) => {
    from = from * 1000;
    to = to * 1000;
    const marks: Array<TimescaleMark> = [];
    this.trades.filter(trade => trade.currencyPair[0] === symbolInfo.full_name.split('/')[0])
      .filter(trade => trade.openTime >= from && (!trade.closeTime ? true : trade.closeTime <= to))
      .forEach(trade => {
        marks.push({
          id: trade.openTime + trade.currencyPair[0],
          time: Math.ceil(trade.openTime / 1000),
          color: 'green',
          label: 'O',
          tooltip: ['Open position on ' + trade.currencyPair[0],
            'Time: ' + moment(trade.openTime).zone(0).format('DD/MM/YY HH:mm')],
        });

        if (trade.closeTime) {
          const duration = moment.duration(trade.closeTime - trade.openTime, 'milliseconds').humanize();

          marks.push({
            id: trade.closeTime + trade.currencyPair[0],
            time: Math.ceil(trade.closeTime / 1000),
            color: 'red',
            label: 'C',
            tooltip: ['Close position on ' + trade.currencyPair[0],
              'PNL: ' + (trade.pnl > 0 ? '<span style=\'color:green;\'>' : '<span style=\'color:red;\'>') + trade.pnl.toFixed(4) + '</span>',
              'Fees: ' + trade.fees.toFixed(4),
              'Duration: ' + duration,
              'Orders: ' + trade.orders.length,
              'Quantity: ' + trade.quantity.toFixed(6),
              'Time: ' + moment(trade.closeTime).format('DD/MM/YY HH:mm')],
          });
        }
      });
    console.log("Return marks:", marks)
    onDataCallback(marks);
  };

}
