import { configureStore } from '@reduxjs/toolkit';
import configurationSlice from './features/configurationSlice';
import backtestResultSlice from './features/resultSlice';

const preloadedState = JSON.parse(localStorage.getItem('backtest_configuration') || '{}')

console.log(preloadedState)


export const store = configureStore({
  reducer: {
    configuration: configurationSlice,
    backtestResult: backtestResultSlice,
  },
  preloadedState,
});

store.subscribe(() => {
  localStorage.setItem('backtest_configuration', JSON.stringify({configuration: store.getState().configuration}));
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
