import React from 'react'
import {alpha, ListSubheader, useMediaQuery, useTheme} from "@material-ui/core";
import {AutocompleteRenderGroupParams} from "@material-ui/lab/Autocomplete";
import {ListChildComponentProps, VariableSizeList} from 'react-window';
import {createStyles, makeStyles} from "@material-ui/core/styles";

const LISTBOX_PADDING = 8; // px

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            display: 'flex',
            alignItems: 'center',
            paddingLeft: 5,
            '&:hover': {
                backgroundColor: alpha(theme.palette.primary.light, 0.5)
            }
        }}
    )
)

function RenderRow(props: ListChildComponentProps) {
    const {data, index, style} = props;
    const classes = useStyles()
    return React.cloneElement(data[index], {
        style: {
            ...style,
            top: (style.top as number) + LISTBOX_PADDING,
        },
        className:classes.root
    });
}

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef<HTMLDivElement>((props, ref) => {
    const outerProps = React.useContext(OuterElementContext);
    return <div ref={ref} {...props} {...outerProps} />;
});

function useResetCache(data: any) {
    const ref = React.useRef<VariableSizeList>(null);
    React.useEffect(() => {
        if (ref.current != null) {
            ref.current.resetAfterIndex(0, true);
        }
    }, [data]);
    return ref;
}

// Adapter for react-window
export const LimitedListComponent = React.forwardRef<HTMLDivElement>(function (props, ref) {
    const {children, ...other} = props;
    const itemData = React.Children.toArray(children);
    const theme = useTheme();
    const smUp = useMediaQuery(theme.breakpoints.up('sm'), {noSsr: true});
    const itemCount = itemData.length;
    const itemSize = smUp ? 36 : 48;

    const getChildSize = (child: React.ReactNode) => {
        if (React.isValidElement(child) && child.type === ListSubheader) {
            return 48;
        }

        return itemSize;
    };

    const getHeight = () => {
        if (itemCount > 8) {
            return 8 * itemSize;
        }
        return itemData.map(getChildSize).reduce((a, b) => a + b, 0);
    };

    const gridRef = useResetCache(itemCount);

    return (
        <div ref={ref}>
            <OuterElementContext.Provider value={other}>
                <VariableSizeList
                    itemData={itemData}
                    height={getHeight() + 2 * LISTBOX_PADDING}
                    width="100%"
                    ref={gridRef}
                    style={{overflowX: 'hidden'}}
                    outerElementType={OuterElementType}
                    innerElementType="ul"
                    itemSize={(index) => getChildSize(itemData[index])}
                    overscanCount={5}
                    itemCount={itemCount}
                >
                    {RenderRow}
                </VariableSizeList>
            </OuterElementContext.Provider>
        </div>
    );
});

export const LimitedListRenderGroup = (params: AutocompleteRenderGroupParams) => [
    <ListSubheader key={params.key} component="div" >
        {params.group}
    </ListSubheader>,
    params.children,
];
